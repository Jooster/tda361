"Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

"Use .vimrc from working directory
set exrc
set secure
let g:netrw_liststyle=3
let mapleader=" "
map <F7> :Lexplore<cr>
let g:netrw_winsize=20

"Project-specific options:
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set colorcolumn=100
highlight ColorColumn ctermbg=darkgray
let &path.="/usr/include,"

"YouCompleteMe
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"

"Appearance
set background=dark
colorscheme elflord

"Key Mappings
set langmap=hi,ik,kj,jh
map <c-l> :tabnext<cr>
map <c-h> :tabprevious<cr> 
map cn <esc>:cn<cr>
map cp <esc>:cp<cr>
nmap <F8> :TagbarToggle<cr>
"autocomplete parenthesis
inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}
inoremap (      ()<Left>
inoremap (<CR>  (<CR>)<Esc>O
inoremap ((     (
inoremap ()     ()
inoremap [      []<Left>
inoremap [<CR>  [<CR>]<Esc>O
inoremap [[     [
inoremap []     []
inoremap "      ""<Left>
inoremap "<CR>  "<CR>"<Esc>O
inoremap ""     ""



"Make
set makeprg=make\
nnoremap <F4> :make! -C ../<cr>
nnoremap <F5> :!./bin/myRTS<cr>
