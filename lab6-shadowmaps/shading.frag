#version 420

// required by GLSL spec Sect 4.5.3 (though nvidia does not, amd does)
precision highp float;

///////////////////////////////////////////////////////////////////////////////
// Material
///////////////////////////////////////////////////////////////////////////////
uniform vec3 material_color;
uniform float material_reflectivity;
uniform float material_metalness;
uniform float material_fresnel;
uniform float material_shininess;
uniform float material_emission;

uniform int has_emission_texture;
layout(binding = 5) uniform sampler2D emissiveMap;

///////////////////////////////////////////////////////////////////////////////
// Environment
///////////////////////////////////////////////////////////////////////////////
layout(binding = 6) uniform sampler2D environmentMap;
layout(binding = 7) uniform sampler2D irradianceMap;
layout(binding = 8) uniform sampler2D reflectionMap;
uniform float environment_multiplier;

///////////////////////////////////////////////////////////////////////////////
// Light source
///////////////////////////////////////////////////////////////////////////////
uniform vec3 point_light_color = vec3(1.0, 1.0, 1.0);
uniform float point_light_intensity_multiplier = 50.0;

///////////////////////////////////////////////////////////////////////////////
// Constants
///////////////////////////////////////////////////////////////////////////////
#define PI 3.14159265359

///////////////////////////////////////////////////////////////////////////////
// Input varyings from vertex shader
///////////////////////////////////////////////////////////////////////////////
in vec2 texCoord;
in vec3 viewSpaceNormal;
in vec3 viewSpacePosition;
in vec4 shadowMapCoord;

///////////////////////////////////////////////////////////////////////////////
// Input uniform variables
///////////////////////////////////////////////////////////////////////////////
uniform mat4 viewInverse;
uniform vec3 viewSpaceLightPosition;
uniform vec3 viewSpaceLightDir;
uniform float spotOuterAngle;
uniform float spotInnerAngle;

///////////////////////////////////////////////////////////////////////////////
// Output color
///////////////////////////////////////////////////////////////////////////////
layout(location = 0) out vec4 fragmentColor;
//layout(binding = 10) uniform sampler2D shadowMapTex;
layout(binding = 10) uniform sampler2DShadow shadowMapTex;



vec3 calculateDirectIllumiunation(vec3 wo, vec3 n)
{
	vec3 wi = normalize(viewSpaceLightPosition - viewSpacePosition);
	if(dot(n, wi) <= 0.0) { return vec3(0.0); }
	float d = length(viewSpaceLightPosition - viewSpacePosition);
	vec3 Li = point_light_intensity_multiplier * point_light_color * (1.0f/pow(d, 2));

	vec3 diffuse_term = material_color * (1.0f / PI) * abs(dot(n, wi)) * Li; 

	float Ro = material_fresnel;
	vec3 wh = normalize(wi + wo);
	float S = material_shininess;

	float F = Ro + ((1.0 - Ro) * pow(1.0 - dot(n, wi), 5));

	float D = ((S + 2.0) / (2.0 * PI)) * pow(dot(n, wh), S);

	float gFirst = 2.0 * ((dot(n, wh) * dot(n, wo)) / dot(wo, wh));
	float gSecond = 2.0 * ((dot(n, wh) * dot(n, wi)) / dot(wo, wh));

	float G = min(1.0, min(gFirst, gSecond));

	float brdf = (F * D * G) / (4*dot(n, wo) * dot(n, wi));

	vec3 dielectric_term = brdf * dot(n, wi) * Li + ((1.0 - F) * diffuse_term);
	vec3 metal_term = brdf * material_color * dot(n, wi) * Li;
	float m = material_metalness;
	vec3 microfacet_term = m * metal_term + ((1 - m) * dielectric_term);
	float r = material_reflectivity;	
	return (r * microfacet_term + ((1.0 - r) * diffuse_term));
}

vec3 calculateIndirectIllumination(vec3 wo, vec3 n)
{
	vec3 dir = vec3(viewInverse * vec4(n, 0.0));

	float theta = acos(max(-1.0f, min(1.0f, dir.y)));
	float phi = atan(dir.z, dir.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	vec2 lookup = vec2(phi / (2.0 * PI), theta / PI);
	vec3 irradiance = environment_multiplier * texture(irradianceMap, lookup).xyz;

	vec3 diffuse_term = material_color * (1.0 / PI) * irradiance;

	vec3 wi = reflect(-wo, n);
	wi = vec3(viewInverse * vec4(wi, 0.0));
	float F = material_fresnel + ((1.0 - material_fresnel) * pow(1.0 - dot(n, wi), 5));
	float S = material_shininess;
	float roughness = pow(2/(S + 2), 0.25);

	theta = acos(max(-1.0f, min(1.0f, wi.y)));
	phi = atan(wi.z, wi.x);
	if (phi < 0.0f) phi = phi + 2.0f * PI;
	// Use these to lookup the color in the environment map
	lookup = vec2(phi / (2.0 * PI), theta / PI);

	vec3 Li = environment_multiplier * textureLod(reflectionMap, lookup, roughness * 7.0).xyz;
	vec3 dielectric_term = F*Li + (1.0 - F) * diffuse_term;
	vec3 metal_term = F * material_color * Li;

	float m = material_metalness;
	vec3 microfacet_term = m * metal_term + ((1 - m) * dielectric_term);
	float r = material_reflectivity;	
	return (r * microfacet_term + ((1.0 - r) * diffuse_term));
}

void main() 
{
	//float depth = texture(shadowMapTex, shadowMapCoord.xy / shadowMapCoord.w).x;
	//float visibility = (depth >= (shadowMapCoord.z / shadowMapCoord.w)) ? 1.0 : 0.0;
	float visibility = textureProj(shadowMapTex, shadowMapCoord);
	float attenuation = 1.0;

	vec3 posToLight = normalize(viewSpaceLightPosition - viewSpacePosition);
	float cosAngle = dot(posToLight, -viewSpaceLightDir);
	//float spotAttenuation = (cosAngle > spotOuterAngle) ? 1.0 : 0.0;
	float spotAttenuation = smoothstep(spotOuterAngle, spotInnerAngle, cosAngle);
	visibility *= spotAttenuation;	

	vec3 wo = -normalize(viewSpacePosition);
	vec3 n = normalize(viewSpaceNormal);

	// Direct illumination
	vec3 direct_illumination_term = visibility * calculateDirectIllumiunation(wo, n);

	// Indirect illumination
	vec3 indirect_illumination_term = calculateIndirectIllumination(wo, n);

	///////////////////////////////////////////////////////////////////////////
	// Add emissive term. If emissive texture exists, sample this term.
	///////////////////////////////////////////////////////////////////////////
	vec3 emission_term = material_emission * material_color;
	if (has_emission_texture == 1) {
		emission_term = texture(emissiveMap, texCoord).xyz;
	}

	vec3 shading = 
		direct_illumination_term +
		indirect_illumination_term +
		emission_term;

	fragmentColor = vec4(shading, 1.0);
	return;

}
